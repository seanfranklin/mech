const gulp = require('gulp'),
    concat = require('gulp-concat'),
sourcemaps = require('gulp-sourcemaps'),
    terser = require('gulp-terser');

gulp.task('styles', function () {
    const srcFiles = [
        'src/styles/main.css',
        'src/styles/mech.css',
        'src/styles/content.css'
    ];
    return gulp.src(srcFiles)
        .pipe(concat('mech.css'))
        .pipe(gulp.dest('www/'));
});

gulp.task('scripts', function () {
    const srcFiles = [
        'src/scripts/content.js',
        'src/scripts/elem.js',
        'src/scripts/mech.js',
        'src/scripts/roll.js',
        'src/scripts/main.js'
    ];
    return gulp.src(srcFiles)
        .pipe(sourcemaps.init())
        .pipe(terser())
        .pipe(concat('mech.min.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('www/'));
});
